from distutils.core import setup
 
setup(
  name="domain_checker",
  version="1.0.0",
  py_modules=["domain_checker"],
  scripts=["domain_checker.py"]
)