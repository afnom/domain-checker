#!/usr/bin/env python3

import re
import json
import whois
import ssl
import urllib3
import requests
import socket
import argparse


import pprint
from pprint import pformat

from urllib.parse import urlparse

from pygments import highlight
from pygments.formatters import TerminalFormatter
from pygments.lexers import JsonLexer


class DomainInfo:

    def __init__(self, domain, suspicion_score=0, certstream_message=None):
        self.domain = domain

        urllib3.disable_warnings()

        if suspicion_score:
            self.suspicion_score = suspicion_score

        if certstream_message:
            cert = certstream_message['data']['leaf_cert']

            self.issuer = cert['issuer']['O']
            self.subject = cert['subject']['CN']
            self.subject_org = cert['subject']['O']

    def perform_all_checks(self):
        """Perform all our checks and populate relevant fields"""

        self.perform_quick_checks()

        self.censys_http_code = self.check_censys_ua_http_response()

        self.linked_urls = self.check_linked_urls()
        self.linked_domains = self.check_linked_domains()
    
    def perform_quick_checks(self):
        """Just perform small subset of possible checks"""

        self.ip = self.check_ip()

        self.whois = self.check_whois()

        self.http_code, self.http_headers, self.http_text = self.check_http_response()

    def check_https_cert_subj(self):
        """Often there's a mismatch between cert and domain"""
        pass

    def check_http_response(self):
        """Try to get the web page and record code, headers and content"""

        url = f'https://{self.domain}'
        headers = {'User-Agent': "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)"}

        try:
            resp = requests.get(url, headers=headers, verify=False, timeout=3)
            return (resp.status_code, resp.headers, resp.text)
        except requests.exceptions.Timeout:
            return (408, {}, "Timeout")
        except requests.exceptions.ConnectionError:
            return (000, {}, "NXDOMAIN")

    def check_censys_ua_http_response(self):
        """Checks the reponse code if we pretend to be censys"""

        # TODO: Not sure if this is how these sites blacklist censys, they might use IP ranges

        # https://support.censys.io/hc/en-us/articles/360043177092-Opt-Out-of-Scanning
        url = f'https://{self.domain}'
        headers = {'User-Agent': "Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)"}

        status_code = None
        try:
            resp = requests.get(url, verify=False, timeout=3)
            status_code = resp.status_code
        except requests.exceptions.Timeout:
            status_code = 408
        except requests.exceptions.ConnectionError:
            status_code = 000
        
        return status_code

    def check_linked_urls(self):
        """Get all URLs referenced from site"""

        # TODO: search for hrefs to same site too??

        body = self.http_text
        headers = self.http_headers
        # TODO: remove query params?
        url_regex = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

        if not (body and headers):
            (_,headers,body) = self.check_http_response()

        urls = re.findall(url_regex, body)
        urls += re.findall(url_regex, str(headers))

        return urls

    def check_linked_domains(self):
        """Summarise other domains referenced from this site: should be interesting to take a look at"""

        # TODO: unfuck
        domain_list = [urlparse(url).netloc for url in self.check_linked_urls()]
        domains = {domain:domain_list.count(domain) for domain in domain_list}

        return domains

    def check_ip(self):
        """Returns ip of domain"""
        
        
        try:
            ip = socket.gethostbyname(self.domain)
        except socket.gaierror:
            ip = "NXDOMAIN"
        
        return ip

    def check_whois(self):
        
        whois_info = {}
        whois_info_verbose = whois.whois(self.domain)
        
        fields = ["domain_name", "registrar", "emails", "org", "referral_url"]
        
        for field in fields:
            try:
                whois_info[field] = whois_info_verbose[field]
            except KeyError:
                pass
        
        return whois_info

    def dict(self):
        return self.__dict__

    def pretty_print(self, include_http_body=False):

        json = self.json(include_http_body)

        return highlight(json, JsonLexer(), TerminalFormatter())

    def json(self, include_http_body=False):

        dict = self.dict()
        if include_http_body == False and len(dict['http_text']) > 20:
            dict['http_text'] = f"{dict['http_text']:40.40}..."

        return json.dumps(dict, indent=4, default=str)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument('domain', help='Domain to analyse')

    parser.add_argument('-j', '--json', action='store_true', help='Dump raw json')
    parser.add_argument('-f', '--full', action='store_true', help='Include entire http body even if not dumping to json')
    parser.add_argument('-q', '--quick', action='store_true', help='Only perform quick checks')
    
    args = parser.parse_args()
    
    domain = DomainInfo(args.domain)

    if args.quick:
        domain.perform_quick_checks()
    else:
        domain.perform_all_checks()

    if args.json:
        print(domain.json())
    else:
        print(domain.pretty_print(include_http_body=args.full))
